
import re
import argparse
from argparse import ArgumentDefaultsHelpFormatter
from datetime import datetime
from operator import attrgetter
from pathlib import Path


class SortedMenu(ArgumentDefaultsHelpFormatter):
    def add_arguments(self, actions):
        actions = sorted(actions, key=attrgetter("option_strings"))
        super(SortedMenu, self).add_arguments(actions)


def get_parser():
    return argparse.ArgumentParser(
        formatter_class=SortedMenu,
        description="Copyright (c) 2020 The Computational Story Lab. Licensed under the MIT License.",
    )


def valid_timescale(t):
    try:
        match = re.match("[1-9][W,M,Y]", t)
        if match:
            return t
        else:
            raise argparse.ArgumentTypeError(f"Timescale format should be [1-9][M,Y]")
    except ValueError:
        raise argparse.ArgumentTypeError(f"Invalid timescale: '{t}'")


def valid_windowsize(w):
    try:
        w = int(w)
        if w > 0:
            return w
        else:
            raise argparse.ArgumentTypeError("Window size must be a positive number!")
    except ValueError:
        raise argparse.ArgumentTypeError(f"Invalid window size: '{w}'")


def valid_date(d):
    try:
        return datetime.strptime(d, "%Y-%m-%d")
    except ValueError:
        raise argparse.ArgumentTypeError(f"Invalid date format: '{d}'")


def parse_args(args):
    parser = get_parser()

    subparsers = parser.add_subparsers(help='Arguments for specific action.', dest='dtype')
    subparsers.required = True

    amt = subparsers.add_parser(
        'amt',
        help='fetch timeseries for AMT ngrams'
    )
    amt.add_argument(
        "data",
        help="path to data directory",
        type=Path
    )
    amt.add_argument(
        "--start_date",
        help="starting date for the query",
        default=datetime(2010, 1, 1),
        type=valid_date,
    )
    amt.add_argument(
        "--end_date",
        help="ending date for the query",
        default=datetime(2020, 12, 1),
        type=valid_date,
    )

    fetch = subparsers.add_parser(
        'fetch',
        help='fetch timeseries for a list of ngrams'
    )
    fetch.add_argument(
        "ngrams_path",
        help="path to text file",
        type=Path
    )
    fetch.add_argument(
        "save_path",
        help="path to text save timeseries",
        type=Path
    )
    fetch.add_argument(
        "--start_date",
        help="starting date for the query",
        default=datetime(2010, 1, 1),
        type=valid_date,
    )

    fetch.add_argument(
        "--end_date",
        help="ending date for the query",
        default=datetime(2020, 12, 1),
        type=valid_date,
    )

    return parser.parse_args(args)
