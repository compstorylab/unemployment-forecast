
import time
import utils
from cli import parse_args


def main(args=None):
    timeit = time.time()

    args = parse_args(args)

    if args.dtype == 'amt':
        for n in range(1, 3):
            ngrams = utils.load_amt_data(args.data/"amt"/"survey.tsv", ngrams=n).index.values

            utils.fetch_ngrams_timeseries(
                list(ngrams),
                database=f"{n}grams",
                savepath=args.data / "amt" / "timeseries" / f"{n}grams",
                start_date=args.start_date,
                end_date=args.end_date,

            )
    elif args.dtype == 'fetch':
        ngrams = utils.load_text(args.ngrams_path)
        utils.fetch_ngrams_timeseries(
            list(ngrams),
            savepath=args.save_path,
            start_date=args.start_date,
            end_date=args.end_date,
        )

    else:
        print('Error: unknown action!')

    print(f"Total time elapsed: {time.time() - timeit:.2f} sec.")


if __name__ == "__main__":
    main()
