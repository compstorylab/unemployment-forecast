
import pandas as pd
import numpy as np
from datetime import datetime
from pathlib import Path

from storywrangling import Storywrangler


def load_amt_data(
    survey_path: str,
    ngrams: int = 0
) -> pd.DataFrame:
    """ Load AMT ratings into a dataframe

    Args:
        survey_path: path to survey data
        ngrams: (0) for both, (1) for 1grams, and (2) for 2grams

    Returns: (pd.DataFrame)
        a dataframe of ngrams and their AMT ratings
    """
    ratings = pd.read_csv(
        survey_path,
        header=0,
        index_col=0,
        sep='\t',
    )

    # Normalize dataframe: (10) is the max number of votes for each ngram on AMT
    #ratings = ratings.div(ratings.sum(axis=1), axis=0)
    ratings = ratings.div(10, axis=0)

    ratings['n'] = ratings.index.str.split().str.len()

    if ngrams is not None:
        return ratings[ratings['n'] == ngrams]
    else:
        return ratings


def load_text(path: str) -> list:
    with open(path) as f:
        file = f.readlines()
    return [line.rstrip('\n') for line in file]


def fetch_ngrams_timeseries(
    ngrams: list,
    savepath: str,
    start_date: datetime = datetime(2010, 1, 1),
    end_date: datetime = datetime(2020, 12, 1)
) -> None:

    """ Ping the Storywrangler API to get ngrams timeseries
    Args:
        ngrams: a list of ngrams
        savepath: path to save timeseries
        start_date: starting date for ngrams timeseries
        end_date: ending date for ngrams timeseries

    """
    storywrangler = Storywrangler()

    df = storywrangler.get_ngrams_array(
        ngrams,
        lang="en",
        start_time=start_date,
        end_time=end_date,
    )
    for cc in df.columns.values:
        d = df.reset_index().pivot_table(index='time', columns='ngram', values=cc, aggfunc='mean')
        file = Path(f'{savepath}/{cc}.tsv.gz')

        if file.exists():
            old = pd.read_csv(file, header=0, index_col=0, na_filter=False, sep='\t')
            old[old == ''] = np.nan

            old = old.combine_first(d)
            old.to_csv(file, sep='\t')
        else:
            file.parent.mkdir(parents=True, exist_ok=True)
            d.to_csv(file, sep='\t')
